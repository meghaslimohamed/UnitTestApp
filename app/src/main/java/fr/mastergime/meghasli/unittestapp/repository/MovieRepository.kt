package fr.mastergime.meghasli.unittestapp.repository

import fr.mastergime.meghasli.unittestapp.data.local.model.Movie
import retrofit2.Response

interface MovieRepository {

    suspend fun getPopularMovies(page : Int): Response<List<Movie>>

}