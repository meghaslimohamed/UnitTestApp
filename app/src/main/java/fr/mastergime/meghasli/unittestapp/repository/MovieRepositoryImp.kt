package fr.mastergime.meghasli.unittestapp.repository

import android.util.Log
import fr.mastergime.meghasli.unittestapp.data.local.model.Movie
import fr.mastergime.meghasli.unittestapp.data.remote.MovieApi
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MovieRepositoryImp @Inject constructor(
    private val movieApi: MovieApi
    ) : MovieRepository {

    override suspend fun getPopularMovies(page : Int): Response<List<Movie>> {
        Log.d("Movies : ", "${movieApi.getPopularMovies(page = page)}")
        return movieApi.getPopularMovies(page = page)
    }

}