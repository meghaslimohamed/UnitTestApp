package fr.mastergime.meghasli.unittestapp.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import fr.mastergime.meghasli.unittestapp.data.local.model.Movie
import fr.mastergime.meghasli.unittestapp.repository.MovieRepositoryImp
import fr.mastergime.meghasli.unittestapp.utils.DataState
import fr.mastergime.meghasli.unittestapp.utils.Status
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MovieViewModel @Inject constructor(
    private val movieRepositoryImp: MovieRepositoryImp
) : ViewModel() {

    val popularMovies = MutableLiveData<DataState<List<Movie>>>()

    fun getPopularMovies(){
        viewModelScope.launch(Dispatchers.IO) {
            //val movieList = mutableListOf<Movie>()
            val movieList = movieRepositoryImp.getPopularMovies(1)
            //movieList.addAll(movieRepositoryImp.getPopularMovies(1))
            //movieList.addAll(movieRepositoryImp.getPopularMovies(2))
            //movieList.addAll(movieRepositoryImp.getPopularMovies(3))
            //movieList.addAll(movieRepositoryImp.getPopularMovies(4))
            //movieList.addAll(movieRepositoryImp.getPopularMovies(5))
            popularMovies.postValue(DataState.Success(movieList.body()!!,Status.SUCCESS))
        }
    }
}