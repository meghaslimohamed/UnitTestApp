package fr.mastergime.meghasli.unittestapp.data.local.model

import androidx.recyclerview.widget.DiffUtil

data class Movie(val title : String, val backdrop_path : String, val vote_average : Float){
    class DiffCallback : DiffUtil.ItemCallback<Movie>(){

        override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean {
            return oldItem.title == newItem.title
        }

        override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean {
            return oldItem.title == newItem.title &&
                    oldItem.vote_average == newItem.vote_average &&
                    oldItem.backdrop_path == newItem.backdrop_path
        }

    }
}