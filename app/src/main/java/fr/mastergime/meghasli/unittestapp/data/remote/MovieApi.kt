package fr.mastergime.meghasli.unittestapp.data.remote

import android.media.Image
import fr.mastergime.meghasli.unittestapp.data.local.model.Movie
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface MovieApi {

    @GET("popular?")
    suspend fun getPopularMovies(
        @Query("api_key") api_key: String = API_KEY,
        @Query("language") language : String = DEFAULT_LANGUAGE,
        @Query("page") page : Int = 1
    ): Response<List<Movie>>

    @GET()
    suspend fun getPoster(

    ):Image

    companion object {
        private const val API_KEY = "1b154ee3f82abacce64a9a7477164655"
        private const val DEFAULT_LANGUAGE = "fr-FR"
    }

}