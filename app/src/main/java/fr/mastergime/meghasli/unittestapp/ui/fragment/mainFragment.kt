package fr.mastergime.meghasli.unittestapp.ui.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import fr.mastergime.meghasli.unittestapp.R
import fr.mastergime.meghasli.unittestapp.databinding.FragmentMainBinding
import fr.mastergime.meghasli.unittestapp.utils.MovieListAdapter
import fr.mastergime.meghasli.unittestapp.viewModel.MovieViewModel

@AndroidEntryPoint
class MainFragment : Fragment() {

    lateinit var binding: FragmentMainBinding
    private val movieViewModel : MovieViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val movieListAdapter = MovieListAdapter(requireContext())

        binding.MovieRecycler.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = movieListAdapter
        }

        binding.updateButton.setOnClickListener {
            movieViewModel.getPopularMovies()
        }

        movieViewModel.popularMovies.observe(viewLifecycleOwner){popularMovies ->
            movieListAdapter.submitList(popularMovies.data)
        }

    }
}