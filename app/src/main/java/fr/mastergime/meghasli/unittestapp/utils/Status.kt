package fr.mastergime.meghasli.unittestapp.utils

    enum class Status {
        SUCCESS,
        ERROR,
        LOADING
    }