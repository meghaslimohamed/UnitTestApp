package fr.mastergime.meghasli.unittestapp.utils

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import fr.mastergime.meghasli.unittestapp.R
import fr.mastergime.meghasli.unittestapp.data.local.model.Movie

class MovieListAdapter(val context: Context) : ListAdapter<Movie, MovieListAdapter.MovieItemViewHolder>(
Movie.DiffCallback()
) {

    inner class MovieItemViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
                var title : TextView = itemView.findViewById(R.id.movieTitle)
                var rate : TextView = itemView.findViewById(R.id.movieRate)
                var icon : ImageView = itemView.findViewById(R.id.movieIcon)
                var rootLayout : LinearLayout = itemView.findViewById(R.id.rootLayout)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieItemViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.movie_item_view_holder,
            parent,false)
        return MovieItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: MovieItemViewHolder, position: Int) {
        holder.apply {
            title.text = getItem(position).title
            rate.text = getItem(position).vote_average.toString()
            Glide.with(context).load("http://image.tmdb.org/t/p/w500/"+getItem(position).backdrop_path)
                .into(icon)

        }

    }


}