package fr.mastergime.meghasli.unittestapp.data.remote.model

import com.squareup.moshi.FromJson
import fr.mastergime.meghasli.unittestapp.data.local.model.Movie

data class PopularMoviesResultJson(val results : List<MovieJson>)

data class MovieJson(val title : String, val backdrop_path : String, val vote_average : Float)

data class MockedResponseCouche(val texte : PopularMoviesResultJson)

class PopularMoviesMoshiAdapter{

    @FromJson
    fun fromJson(popularMoviesResultJson: PopularMoviesResultJson):List<Movie>{
        return popularMoviesResultJson.results.map { MovieJson ->
            Movie(MovieJson.title,MovieJson.backdrop_path, MovieJson.vote_average)
        }
    }
}

class PopularMoviesMoshiAdapterForMockedResponse{

    @FromJson
    fun fromJson(mockedResponseCouch: MockedResponseCouche):List<Movie>{
        return mockedResponseCouch.texte.results.map {
                MovieJson ->  Movie(MovieJson.title,MovieJson.backdrop_path, MovieJson.vote_average)
        }
    }
}

