package fr.mastergime.meghasli.unittestapp.viewModel
import android.util.Log
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import fr.mastergime.meghasli.unittestapp.data.local.model.Movie
import fr.mastergime.meghasli.unittestapp.data.remote.MovieApi
import fr.mastergime.meghasli.unittestapp.data.remote.model.PopularMoviesMoshiAdapter
import fr.mastergime.meghasli.unittestapp.data.remote.model.PopularMoviesMoshiAdapterForMockedResponse
import fr.mastergime.meghasli.unittestapp.repository.MovieRepositoryImp
import fr.mastergime.meghasli.unittestapp.utils.getOrAwaitValue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okio.BufferedSource
import okio.buffer
import okio.source
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.nio.charset.StandardCharsets
import java.util.concurrent.TimeUnit

@ExperimentalCoroutinesApi
//@ExperimentalSerializationApi
@RunWith(AndroidJUnit4::class)
@SmallTest
class MovieViewModelTest {
    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var mockWebServer: MockWebServer
    private lateinit var popularMoviesMoshiAdapter: Moshi
    private lateinit var popularMoviesMoshiAdapterForMockedResponse : Moshi
    private lateinit var client: OkHttpClient
    private lateinit var movieApi: MovieApi
    private lateinit var movieRepositoryImp : MovieRepositoryImp
    private lateinit var movieViewModel: MovieViewModel

    @Before
    fun setup(){
        mockWebServer = MockWebServer()

        popularMoviesMoshiAdapter = Moshi.Builder().add(KotlinJsonAdapterFactory())
            .add(PopularMoviesMoshiAdapter()).build()

        popularMoviesMoshiAdapterForMockedResponse = Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .add(PopularMoviesMoshiAdapterForMockedResponse()).build()

        client = OkHttpClient.Builder()
            .connectTimeout(1, TimeUnit.SECONDS)
            .readTimeout(1, TimeUnit.SECONDS)
            .writeTimeout(1, TimeUnit.SECONDS)
            .build()

        movieApi = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .client(client)
            .addConverterFactory(MoshiConverterFactory.create(popularMoviesMoshiAdapter))
            .build()
            .create(MovieApi::class.java)

        movieRepositoryImp = MovieRepositoryImp(movieApi)
        movieViewModel = MovieViewModel(movieRepositoryImp)
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }


    @ExperimentalStdlibApi
    @Test
    fun shouldFetchMoviesCorrectlyGiven200response() {

        for(i in 1..2){
            mockWebServer.enqueueResponse("popular-movies.json", 200)
        }


        runBlocking {
            movieViewModel.getPopularMovies()
            //val actual = movieRepositoryImp.getPopularMovies(1)
            val actual = movieViewModel.popularMovies.getOrAwaitValue().data
            /*val mockedResponse = MockResponse().setResponseCode(200).setBody(
                mockFileReader("popular-movies.json")?.readString(StandardCharsets.UTF_8)
            )*/
            //Log.d("MockResponseText : ","${mockFileReader("popular-movies.json")?.readString(StandardCharsets.UTF_8)}")

            //val  jsonAdapter : JsonAdapter<Movie> = popularMoviesMoshiAdapter.adapter<Movie>()
            //val expected2 = jsonAdapter.fromJson(mockFileReader("popular-movies.json"))

            //Log.d("MockResponseMoshed2 : ","$expected2")

            val expected = listOf(
                Movie(title="Spider-Man: No Way Home", backdrop_path="/1Rr5SrvHxMXHu5RjKpaMba8VTzi.jpg", vote_average=8.4f),
                Movie(title="Encanto, la fantastique famille Madrigal", backdrop_path="/3G1Q5xF40HkUBJXxt2DQgQzKTp5.jpg", vote_average=7.8f),
                Movie(title="Resident Evil : Bienvenue à Raccoon City", backdrop_path="/o76ZDm8PS9791XiuieNB93UZcRV.jpg", vote_average=6.1f),
                Movie(title="Matrix Resurrections", backdrop_path="/hv7o3VgfsairBoQFAawgaQ4cR1m.jpg", vote_average=7.1f),
                Movie(title="Venom : Let There Be Carnage", backdrop_path="/vIgyYkXkg6NC2whRbYjBD7eb3Er.jpg", vote_average=7.2f),
                Movie(title="Red Notice", backdrop_path="/dK12GIdhGP6NPGFssK2Fh265jyr.jpg", vote_average=6.8f),
                Movie(title="Shang-Chi et la Légende des Dix Anneaux", backdrop_path="/cinER0ESG0eJ49kXlExM0MEWGxW.jpg", vote_average=7.8f),
                Movie(title="Fortress", backdrop_path="/1BqX34aJS5J8PefVnQSfQIEPfkl.jpg", vote_average=6.4f),
                Movie(title="Don't Look Up : Déni cosmique", backdrop_path="/nvxrQQspxmSblCYDtvDAbVFX8Jt.jpg", vote_average=7.3f), Movie(title="Tous en scène 2", backdrop_path="/tutaKitJJIaqZPyMz7rxrhb4Yxm.jpg", vote_average=7.6f),
                Movie(title="Clifford", backdrop_path="/1Wlwnhn5sXUIwlxpJgWszT622PS.jpg", vote_average=7.4f),
                Movie(title="The Amazing Spider-Man", backdrop_path="/sLWUtbrpiLp23a0XDSiUiltdFPJ.jpg", vote_average=6.6f),
                Movie(title="Les Éternels", backdrop_path="/lyvszvJJqqI8aqBJ70XzdCNoK0y.jpg", vote_average=7.1f),
                Movie(title="Sooryavanshi", backdrop_path="/gg2w8QYf6o5elN95RHtikQaVIsc.jpg", vote_average=5.5f),
                Movie(title="Spider-Man : Homecoming", backdrop_path="/tTlAA0REGPXSZPBfWyTW9ipIv1I.jpg", vote_average=7.4f),
                Movie(title="Ida Red", backdrop_path="/weneJTnAb1IFI94SKcaXzBFmPKH.jpg", vote_average=5.6f),
                Movie(title="Chernobyl : Under Fire", backdrop_path="/xGrTm3J0FTafmuQ85vF7ZCw94x6.jpg", vote_average=6.2f),
                Movie(title="The Amazing Spider-Man : Le Destin d'un héros", backdrop_path="/u7SeO6Y42P7VCTWLhpnL96cyOqd.jpg", vote_average=6.5f), Movie(title="Harry Potter fête ses 20 ans : retour à Poudlard", backdrop_path="/8rft8A9nH43IReybFtYt21ezfMK.jpg", vote_average=8.6f),
                Movie(title="Invasion", backdrop_path="/zlj0zHo67xXoj7hvwGtaKRkSdBV.jpg", vote_average=6.3f)
            )

            assertThat(expected).isEqualTo(actual)
        }
    }

    private fun mockFileReader(fileName: String): BufferedSource? {
        val inputStream = javaClass.classLoader?.getResourceAsStream("assets/$fileName")
        return inputStream?.source()?.buffer()
    }

    private fun MockWebServer.enqueueResponse(fileName: String, code: Int) {

        val source = mockFileReader(fileName)

        if (source != null) {
            enqueue(
                MockResponse()
                    .setResponseCode(code)
                    .setBody(source.readString(StandardCharsets.UTF_8))
            )
        }

        Log.d("Source : ","$source")
    }


}